package logo;

public class Smiley extends DecorateurLogo {

	public Smiley(Logo logo) {
		this.accessoire = logo;
	}

	public String getDescription() {
		return  accessoire.getDescription() + "Smiley";
	}
	
	public double cout() {
		return 0.70 + accessoire.getCout();
	}
	
	@Override
	public MyImage getImage() {
		MyImage i = accessoire.getImage();
		i.paintOver("img/Smiley.png", 260,210);
		return i;
	}
}

	

