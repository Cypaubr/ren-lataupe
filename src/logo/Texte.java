package logo;

public class Texte extends DecorateurLogo {

	public Texte(Logo logo) {
		this.accessoire = logo;
	}

	public String getDescription() {
		return  accessoire.getDescription() + "Smiley";
	}
	
	public double cout() {
		return 0.70 + accessoire.getCout();
	}
	
	@Override
	public MyImage getImage() {
		MyImage i = accessoire.getImage();
		i.textOver("Im Rene LaTaupe", 260,210);
		return i;
	}
	
}
