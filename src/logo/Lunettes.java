package logo;

public class Lunettes extends DecorateurLogo {

		public Lunettes(Logo logo) {
			this.accessoire = logo;
		}

		public String getDescription() {
			return  accessoire.getDescription() + "chapeau";
		}
		public double cout() {
			return 0.30 + accessoire.getCout();
		}
		

		@Override
		public MyImage getImage() {
			MyImage i = accessoire.getImage();
			i.paintOver("img/Lunettes.png", 280,42);
			return i;
		}
	}

	

