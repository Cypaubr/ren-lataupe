package logo;

public abstract class Logo {
	String description = null;
	double cout = 0.0;
	
	public String getDescription(){
		return description;
	}
	
	public double getCout(){
		return cout;
	}
	
	public abstract MyImage getImage();
}
