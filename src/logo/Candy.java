package logo;

public class Candy extends DecorateurLogo {

	public Candy(Logo logo) {
		this.accessoire = logo;
	}

	public String getDescription() {
		return  accessoire.getDescription() + "b�ton de sucrerie";
	}
	
	public double cout() {
		return 0.30 + accessoire.getCout();
	}
	
	@Override
	public MyImage getImage() {
		MyImage i = accessoire.getImage();
		i.paintOver("img/Candy.png", 441,202);
		return i;
	}
}
