package logo;


public class ReneLaTaupe extends Logo{
	/**
     * Chemin d'acces au fichier
     * contenant l'image de fond du logo
     */
    private String nomIm;
    
    /**
     * Prix du logo
     */
    private double prix;
    
    /**
     * Constructeur
     */
    public ReneLaTaupe() {
        nomIm = "img/Taupe.jpg";
        prix=3.65;
    }
    
  
    /**
     * @return le prix du logo
     */
    public double combienCaCoute(){
        return prix;
    }

	@Override
	public MyImage getImage() {
		return new MyImage(nomIm);
	}
}
